import pandas as pd
import autosklearn.classification
from sklearn.model_selection import cross_val_score,train_test_split,KFold
from sklearn.metrics import f1_score,precision_score,recall_score,precision_recall_curve,accuracy_score,roc_auc_score,precision_score
import pickle



def main():
	# data=pd.read_csv('cod_user_order.csv',header=None,
	# 				 names=["order_name","label","cod_cnt","cod_delivered","cod_reject",
	# 						"cod_on_delivery","fst_create_days","is_new","is_old","is_email_yaho",
	# 						"amount_total", "sum_origin_qty", "pid_cnt_dst","avg_item_price","is_women",
	# 						"shipping_state_Assam","shipping_state_Delhi","shipping_state_Gujarat","shipping_state_Karnataka",
	# 						"shipping_state_Maharashtra","shipping_state_Telangana","cat1_new_Bags","cat1_new_Daily_Necessities",
	# 						"cat1_new_Electronics","cat1_new_Mens_Clothing","is_cat_male","is_cat_female", "score"])
	# data=pd.read_csv('cod_month_9_1122.csv')
	data=pd.read_csv('s3://jiayun-recommend/private/wumengting/cod/cod_seller_automl_feature.csv/part-00000-6fc247d3-79f3-44ef-8d6a-18892b9bfbb4-c000.csv')
	data=data.fillna(0)
	df = data.drop(['is_sale','catid1','catid2','catid3','seller_id','seller_name','online_store_name','store_id','store_name','new_first_category_id','cate_two_id','major_category_ids','biz_type_algo',
           'create_date','etl_time','log_date','verify_time','open_time','pids','order_name3'], 1)
	X_train, X_test, y_train, y_test = train_test_split(df.drop(['label'],1), df['label'],
                                                    test_size=0.3, random_state=42)
	# feature_types=['numerical']*X_train.shape[1]
	# cat_idx = [23,24]
	# for idx in cat_idx:
	# 	feature_types[idx]='categorical'
	include_estimators = ['xgradient_boosting', ]
	automl = autosklearn.classification.AutoSklearnClassifier(time_left_for_this_task=28800,per_run_time_limit=14400,
															  resampling_strategy='holdout',
															  resampling_strategy_arguments={'train_size': 0.80,'shuffle':True},
															  include_estimators=include_estimators,
															  seed=3,
															  delete_output_folder_after_terminate=True,
															  delete_tmp_folder_after_terminate=True,
															  ensemble_memory_limit=40960,
															  ml_memory_limit=51200,
															  disable_evaluator_output=False,
															  exclude_estimators=None,
															  exclude_preprocessors=None,)
	automl.fit(X_train, y_train)
	# ypred = automl.predict(X_test)
	ypred = automl.predict_proba(X_test)
	result = y_test.reset_index()
	result['pred'] = ypred
	y_true = result['label']
	y_scores = result['pred']
	print("AUC =", roc_auc_score(y_true, y_scores))
	print(automl.show_models())
	print(automl.get_params())
	print(automl.sprint_statistics())
	# result.to_csv('result.csv')
	# roc=roc_auc_score(y_true, y_scores)
	# with open('roc.txt',"a") as f:
		# f.write(str(roc))
	pickle_model = open('cod_user_order.pkl','wb')
	pickle.dump(automl, pickle_model, -1)
	pickle_model.close()
	feature = pd.Series(automl.feature_importance()).sort_values(ascending=False)
	print(feature)





if __name__ == '__main__':
	main()