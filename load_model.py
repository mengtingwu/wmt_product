import pandas as pd
import autosklearn.classification
from sklearn.model_selection import cross_val_score,train_test_split,KFold
from sklearn.metrics import f1_score,precision_score,recall_score,precision_recall_curve,accuracy_score,roc_auc_score,precision_score
import pickle




def load_model():
    with open('cod_user_order.pkl', 'rb') as file:
        cls = pickle.load(file)
        print(cls.get_params())
        print(cls.sprint_statistics())
        feature=pd.Series(cls.feature_importance()).sort_values(ascending=False)
        print(feature)





if __name__ == '__main__':
	load_model()